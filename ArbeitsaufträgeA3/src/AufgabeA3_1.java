
public class AufgabeA3_1 {

	static double berechneMittelwert(double x, double y) {
		return (x + y) / 2.0;
	}

	public static void main(String[] args) {

		double x = 2.0;
		double y = 4.0;
		double Mittelwert = berechneMittelwert(x, y);
		System.out.println(Mittelwert);

	}
}
