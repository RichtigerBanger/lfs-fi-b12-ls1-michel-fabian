import java.util.Scanner;

public class AufgabeA3_2PCHaendler {
	public static double rechnungsausgabe(String artikel,int anzahl, double nettogesamtpreis,double bruttogesamtpreis,double mwst) {
		
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		return 0;
	}

	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		return anzahl * nettopreis;
	}

	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst / 100);
	}

	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("was moechten Sie bestellen?");
		String artikel = myScanner.next();
		return artikel;
	}

	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);

		double preis = myScanner.nextDouble();
		return (preis);
	}

	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		String artikel = liesString(null);

		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = liesInt(null);

		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = liesDouble(null);

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = liesInt(null);

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben

		rechnungsausgabe(artikel,anzahl,nettogesamtpreis,bruttogesamtpreis, mwst);

	}

}