import java.util.Scanner;

public class AufgabeA3_2_3bis4wiederstand {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);

		double widerstand1, widerstand2, widerstand3;
		double parallelwiderstand23, gesamtwiderstand;

		System.out.print("Widerstand1 (Ohm): ");
		widerstand1 = eingabe.nextDouble();

		System.out.print("Widerstand2 (Ohm): ");
		 widerstand2 = eingabe.nextDouble();

		System.out.print("Widerstand3 (Ohm): ");
		widerstand3 = eingabe.nextDouble();

		parallelwiderstand23 = Parallelschaltung(widerstand2, widerstand3);

		gesamtwiderstand = reihenschaltung(widerstand1, parallelwiderstand23);

		System.out.println("Der Gesamtwiderstand betr�gt " + gesamtwiderstand + " Ohm\n");
		System.out.println("Der ErsatztWiederstand der Reihenschalturn von W1 und W2 betr�gt " + reihenschaltung(widerstand1,widerstand2) + " Ohm\n");
		System.out.println("Der Ersatzwiderstand der Parallelschaltung aus W1 und W2 betr�gt " + Parallelschaltung(widerstand1,widerstand2) + " Ohm\n");
		
	}

	static double reihenschaltung(double r1, double r2) {
		double rges = r1 + r2;
		return rges;
	}

	static double Parallelschaltung(double r1, double r2) {
		double rges = 1.0 / (1.0 / r1 + 1.0 / r2);
		return rges;
	}
}

