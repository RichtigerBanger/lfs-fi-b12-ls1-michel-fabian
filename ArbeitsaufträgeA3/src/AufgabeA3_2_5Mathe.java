import java.util.Scanner;

public class AufgabeA3_2_5Mathe {

	public static double hypotenuse(double kathete1, double kathete2) {
		kathete1 = quadrat(kathete1);
		kathete2 = quadrat(kathete2);
		double hyp = Math.sqrt(kathete1 + kathete2);

		return hyp;
	}

	public static double quadrat(double x) {
		x *= x;
		return x;
	}

	public static void main(String[] args) {
		double h = 0;

		Scanner eingabe = new Scanner(System.in);
		System.out.println("Welche Zahl m�chten sie ins Quadrat nehmen");
		double zahl = eingabe.nextDouble();
		System.out.println("Die Quadrahtzahl von " + zahl + " ist " + quadrat(zahl));

		// Aufgabe 7 Katheten
		System.out.println("geben sie die l�nge der 1 Kathete in cm ein ");
		double k1 = eingabe.nextDouble();
		System.out.println("geben sie die l�nge der 2 Kathete in cm ein ");
		double k2 = eingabe.nextDouble();
		h = AufgabeA3_2_5Mathe.hypotenuse(k1, k2);
		System.out.println("Die Hypotenuse w�re in diesem Fall " + h + "cm lang");
	}

}
