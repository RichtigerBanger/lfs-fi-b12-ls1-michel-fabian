﻿import java.util.Scanner;
//stand 8.12 A4.7 muss gemacht werden 
class Fahrkartenautomat {
	public static void main(String[] args) {
		boolean arbeiten = true;
		if(arbeiten = true) {
		double zuZahlenderBetrag;
		zuZahlenderBetrag = fahrkartenbestellungErfassen();		
		fahrkartenBezahlen(zuZahlenderBetrag);
	}
	}

	// Fahrscheinausgabe
	public static void fahrkartenAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Wie lange willst du warten (in milisekunden)");	
		int time = tastatur.nextInt();
		System.out.println("\nFahrschein/e wird ausgegeben");
		warte(time);
		System.out.println("\n\n");

		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
		return;
	}

	// rueckgeldausgabe
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rückgabebetrag;
		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			
			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
		System.out.println("\n bitte warten der automat wird Neu eigerichtet");
		try {
			for(int i=0;i<=7;i++) {
			Thread.sleep(500);
			System.out.print("=");
			}
			System.out.print("\n");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		main(null);
	}

	// fahrkartenbestellungErfassen
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag= 0;
		int ticketart;
		int anzahl;
System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n"
		+ "		Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"
		+ "		Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
		+ "		Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");
		System.out.print("Ticketart: ");
		ticketart = tastatur.nextInt();
		if(ticketart < 1||ticketart>3 ) {
			System.out.println("Ungültiger Eingabewert Ticketart nicht gefunden !!\n\n ");
			main(null);
					
			
		}
		
		switch(ticketart){
        case 1:
        	zuZahlenderBetrag = 2.90 ;
            break;
        case 2:
        	zuZahlenderBetrag = 8.60 ;
        	break;
        case 3 :
        	zuZahlenderBetrag = 23.50;
        	
        	
		}

			
			
		

		System.out.print("Anzahl der Tickets");

		anzahl = tastatur.nextInt();
		
		if(anzahl > 10 || anzahl < 1) {
			anzahl = 1 ;
			System.out.println("Ungültiger Eingabewert standart menge 1 Ticket wird übernommen");
		}
		
		
		zuZahlenderBetrag = zuZahlenderBetrag * anzahl;
		return zuZahlenderBetrag;
	}

	// fahrkartenBezahlen
	public static void fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		Scanner tastatur = new Scanner(System.in);

		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		fahrkartenAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
		return;
	}
	public static void warte(int time) {
		int t = time; 
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(time);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
}

/*
 * zu Aufgabe A2.5 1,2,5 und 6. 1) double zuZahlenderBetrag; double
 * eingezahlterGesamtbetrag; double eingeworfeneMünze; double rückgabebetrag;
 * 2:-=,>=,>,-,+=,*,<,=,++,+
 * 
 * 5: Ich habe einen Integer genommen da es nicht möglich ist halbe tickets zu
 * kaufen.
 * 
 * 6:der Einzelpreis(zuZahlenderBetrag) wird durch * mit der anzahl der Tickets
 * Multipliziert.
 */