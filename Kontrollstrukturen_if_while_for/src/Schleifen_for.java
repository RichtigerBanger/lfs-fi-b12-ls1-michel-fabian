import java.util.Scanner;

public class Schleifen_for {

	public static void main(String[] args) {
		// Creator: Fabian Theodor Michel
		//Date: 26.01.2022

		
		Scanner eingabe = new Scanner(System.in);
		// Aufgabe A 
		System.out.println("wie viele Zahlen m�chtest du miteinander  addieren?");
		int anzahl = eingabe.nextInt();
		int summe = 0;
		for(int i = 1; i <= anzahl ;i++) {
			summe += i;
		}
		System.out.println("Die summe ist"+ summe);
		// Aufgabe B
		System.out.println("wie viele Zahlen m�chtest du  miteinander nur gerade zahlen addieren?");
		int anzahl2 = eingabe.nextInt();
		summe = 0;
		int addierer= 2;
		for(int i = 1; i<= anzahl2; i++) {						
			summe += addierer;	
			addierer += 2;			
		}
		System.out.println("Die summe f�r B ist"+ summe);
		// Aufgabe C
		System.out.println("wie viele Zahlen m�chtest du miteinander nur ungerade zahlen addieren?");
		int anzahl3 = eingabe.nextInt();
		summe = 1;
		int addierer2= 3;
		for(int i = 1; i<= anzahl3; i++) {						
			summe += addierer2;	
			addierer2 += 2;	
	}
		System.out.println("Die suchi f�r C ist"+ summe);
	}
}
